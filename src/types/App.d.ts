interface IAppProps {
    fullName: string;
    skills: Array<Skill>;
    articles: Array<JobArticle|EducationArticle|ProjectArticle>;
    footerLinks: Array<FooterLink>;
}

type ArticleType = "Job" | "Education" | "Project" | "Skill";

interface BaseArticle {
    id: string;
    articleType: ArticleType
}

interface DescriptiveArticle extends BaseArticle {
    title: string;
}

interface Skill {
    id: number;
    name: string;
    category: string;
}

interface JobArticle extends DescriptiveArticle {
    startDate?: string;
    endDate?: string;
    skillIds: Array<number>;
    description: string;
    company: string;
}

interface EducationArticle extends DescriptiveArticle {
    degreeEarned: string;
    yearCompleted: number;
}

interface ProjectArticle extends DescriptiveArticle {
    href: string;
}

interface SkillArticle extends BaseArticle {
    skillId: number;
}

interface FooterLink {
    id: string;
    text: string;
    href: string;
}

interface SectionMapEntry {
    heading: string;
    articles: Array<BaseArticle>,
    articleType: ArticleType,
    className: string;
}
