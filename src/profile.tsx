let ProfileData = {
    "fullName": "Andrew Revak",
    "skills": [
        {id: 1, name: "Java", category: "language"},
        {id: 2, name: "C#", category: "language"},
        {id: 3, name: "JavaScript", category: "language"},
        {id: 4, name: "TypeScript", category: "language"},
        {id: 5, name: "Node.js", category: "language"},
        {id: 6, name: "Python", category: "language"},
        {id: 7, name: "Perl", category: "language"},
        {id: 8, name: "PHP", category: "language"},
        {id: 9, name: "Spring Boot", category: "framework"},
        {id: 10, name: "Lombok", category: "framework"},
        {id: 11, name: "Mockito", category: "testing"},
        {id: 12, name: "Jest", category: "testing"},
        {id: 13, name: "Mockito BDD", category: "testing"},
        {id: 14, name: "JUnit", category: "testing"},
        {id: 15, name: "PHPUnit", category: "testing"},
        {id: 16, name: "Unity", category: "framework"},
        {id: 17, name: "SASS", category: "language"},
        {id: 18, name: "MySQL", category: "db"},
        {id: 19, name: "MongoDB", category: "db"},
        {id: 20, name: "Ubunutu", category: "os"},
        {id: 21, name: "CentOS", category: "os"},
        {id: 22, name: "OSX", category: "os"},
        {id: 23, name: "Windows", category: "os"},
        {id: 24, name: "Ansible", category: "framework"},
        {id: 25, name: "Puppet", category: "framework"},
        {id: 26, name: "PostgreSQL", category: "db"},
        {id: 27, name: "YUI", category: "framework"},
        {id: 28, name: "KendoUI", category: "framework"},
        {id: 29, name: "ExtJS", category: "framework"}
    ],
    "articles": [
        {
            id: "fogbank",
            articleType: "Job",
            title: "Senior Software Engineer",
            description: "I was a senior software engineer on Storyscape.  I contributed in many ways to the project.\n" +
                "\n" +
                "* Did initial setup of Jenkins server and Nginx proxy\n" +
                "* Developed and maintained build pipelines for nearly all the component parts of the project (App, Content Portal, Writing Tool, Episode Bundling, Cloud Code)\n" +
                "* Bootstrapped the framework for cloud code (PlayFab) as well as a number of features in it\n" +
                "* Optimized storage and retrieval of content portal data to scale out\n" +
                "* Architected and scaled out build system for episode bundling (OTA content)\n" +
                "* Architected Ansible infrastructure to scale all our build infrastructure\n" +
                "* Developed a data migration tool to move and validate data across environments (Stage -> Prod for example)\n" +
                "* Began work on a React front end to be an administrative tool that replaces the data migration tool\n" +
                "* Provided detailed technical documentation and designs on work",
            skillIds: [1, 2, 3, 4, 5, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 24],
            startDate: "2018-01-22T00:00:00-0800",
            endDate: "2020-03-24T00:00:00-0800",
            company: "Fogbank Entertainment"
        },
        {
            id: "gazillion-ic",
            articleType: "Job",
            title: "Self-Employed",
            description: "Consulted for partners for growth and worked on a small team to prepare software for liquidation. \n" +
                "\n" +
                "* Worked with a small team to document liquidated Gazillion Assets and prepare it for sale\n" +
                "* Communicated technical architecture details to non technical people\n" +
                "* Provided screen cast walk-throughs of how to use the software\n" +
                "* Met with potential buyers to answer technical due diligence questions",
            skillIds: [],
            startDate: "2017-12-01T00:00:00-0800",
            endDate: "2017-12-15T00:00:00-0800",
            company: "Self-Employed"
        },
        {
            id: "gazillion",
            articleType: "Job",
            title: "Lead Platform Engineer",
            description: "I was the lead platform engineer for Marvel Heroes / Marvel Heroes Omega.  The stack was in PHP & MongoDB\n" +
                "\n" +
                "* Co-developed a set of APIs consumed by many parts of the business\n" +
                "* Responsible for staff facing tools to manage customer and game data\n" +
                "* Developed unit tests to support API development and ensure code correctness\n" +
                "* Built a standalone multi-threaded bulk email microservice\n" +
                "* Participated in code reviews\n" +
                "* Worked with Ops to help deploy infrastructure via Puppet",
            skillIds: [3, 6, 8, 15, 18, 19, 21, 23, 25, 28],
            startDate: "2014-01-01T00:00:00-0800",
            endDate: "2017-11-01T00:00:00-0800",
            company: "Gazillion Inc"
        },
        {
            id: "polyvore",
            articleType: "Job",
            title: "Software Engineer",
            description: "At Polyvore I worked on the Consumer and the Retention team.  Here are some of my accomplishments:\n" +
                "\n" +
                "* Visually updated the profile page from a UI design, worked closely with PM and designer during implementation.  Wrote automated unit tests using Selenium and Mechanize to test functionality.\n" +
                "* Helped maintain a UI library for use by entire team.  Worked with pagination manager and beacon tracking.\n" +
                "*  Implemented A/B testing for upselling registration that led to 15% increase in registrations per visitor.\n" +
                "*  Modified existing A/B Testing framework for use in email so that weekly emails that we sent out could be A/B tested.  Built object oriented email class to simplify email development.\n" +
                "*  Build dashboards to track user registration sources and determine user engagement.\n" +
                "*  Proactively added utilities to improve testing infrastructure to allow Selenium to have cleaner conditions to wait for so that those selenium based tests could be less intermittent.\n" +
                "*  Implemented improvements on email newsletter which resulted in a 4% improvement on click back rate over previous control by making the email more shoppable.\n" +
                "*  Wrote workers using Gearman architecture to process tasks offline, such as generating emails and activity notifications.  Examples include a script which processes user activity and engagement based on beacons and metrics we track.\n" +
                "*  Helped identify and implement opportunities for improving sale alert emails in the existing system.  I audited the logic used to determine recipients and potential actions to take.\n" +
                "*  Test driven development was used for all features developed where possible.\n" +
                "*  Learned the STAR technique for behavioral interviewing techniques for interviewing candidates to help better determine culture fit and improve interview process.\n" +
                "*  Led technical interviews for new candidates.",
            skillIds: [3, 7],
            startDate: "2012-12-01T00:00:00-0800",
            endDate: "2013-10-01T00:00:00-0800",
            company: "Polyvore"
        },
        {
            id: "huddler",
            articleType: "Job",
            title: "Senior Software Engineer",
            description: "While employed by Huddler Inc. I worked as a full stack developer with a front end focus.  I worked on the image galleries project, implementing the gallery uploader and image embedding.\n" +
                "\n" +
                "*  Developed command line scripts\n" +
                "*  Implemented YUI Lazy Loading to load JS on demand for the UI to increase initial page load performance.\n" +
                "*  Contributed to design reviews for existing UIs.\n" +
                "*  Participated in peer code review.\n" +
                "*  Worked on the uploader for the image galleries project and maintenance\n" +
                "*  Worked on the image embed project to allow deep linking of content in multiple contexts of the site to optimize the storage and import of external images\n" +
                "*  Enhanced the thread lightbox\n" +
                "*  Helped to maintain gallery image thumbnailer (ImageMagick)\n" +
                "*  Worked in an Agile environment delivering on sprint deadlines and participating in daily stand-ups.",
            skillIds: [3, 8, 21, 26, 27],
            startDate: "2011-10-01T00:00:00-0800",
            endDate: "2012-11-01T00:00:00-0800",
            company: "Huddler Inc"
        },
        {
            id: "tinyprints",
            articleType: "Job",
            title: "Web Application Developer",
            description: "I primarily developed software to allow web users to personalize products on our site.  My responsibilities included maintenance and development of our code base to allow it to support different kinds of products and integrate with various technologies.\n" +
                "\n" +
                "*  Extensive back-end and front-end development in PHP/JavaScript.  Used ExtJS 3.2.1 to produce a variety of tools supporting the merchandising department.\n" +
                "*  I wrote the API layer on our code base to support connecting to PicasaWeb (Google), Flickr (Yahoo), Facebook and SmugMug.  This unified the network communications necessary to allow our customers to use their photos with our web services for personalizing their products.\n" +
                "*  Integrated LiveChat into the website.\n" +
                "*  Created back-end services to support setup of new products and features for our personalization engine.\n" +
                "*  Integrated our address book API with the personalization engine to support direct mailing services.\n" +
                "*  Maintained scripts for Adobe Illustrator to import data from our database using web service APIs to render the customers’ text on templates for import to our environment.\n" +
                "*  Worked with the merchandising department to improve their work-flow for enabling products.\n" +
                "*  Worked with ImageMagick to produce composite previews on the fly for the customer.\n" +
                "*  Responsible for mentoring junior developers.\n" +
                "*  Responsible for participating in code reviews, interviewing developers, code development and debug, as well as smooth upgrades to the active systems.\n" +
                "*  Worked in an Agile environment delivering on sprint deadlines and participating in daily stand-ups.",
            skillIds: [3, 8, 21],
            startDate: "2008-05-01T00:00:00-0800",
            endDate: "2010-11-01T00:00:00-0800",
            company: "Tiny Prints"
        },
        {
            id: "montalvo",
            articleType: "Job",
            title: "Verification Engineer",
            description: "Developed metrics and supported the existing tools and infrastructure in a high speed microprocessor design verification environment. \n" +
                "\n" +
                "*  Used ExtJS to build a code coverage viewer.\n" +
                "*  Responsible for running regressions to support the cross-site project with more than 30 design verification engineers and numerous block level and full chip test environments. \n" +
                "*  Developed an automated regression scheduling system to allow engineers to submit regression jobs, while maintaining fair sharing of simulation resources between teams working on different blocks. \n" +
                "*  Developed verification planning tools to allow the design verification team to create standardized test plans in XML format. The tools facilitated the organization of test plan items at multiple levels, reviewing of test plans and tracking of current status. Additional tools were developed to access the test plans database to generate metrics for project progress over time. \n" +
                "*  Responsible for periodically generating regression results and test plan completion metrics.\n" +
                "*  Developed tools to facilitate the generation of reports for viewing on an internal website.",
            skillIds: [6, 29, 3],
            startDate: "2007-07-01T00:00:00-0800",
            endDate: "2008-03-01T00:00:00-0800",
            company: "Montalvo Systems"
        },
        {
            id: "csueb",
            articleType: "Education",
            title: "California State University, Hayward (renamed to East Bay)",
            degreeEarned: "Bachelors of Science, Computer Science",
            yearCompleted: 2004
        },
        {
            id: "profile",
            articleType: "Project",
            title: "Portfolio Project",
            href: "https://bitbucket.org/arevak/portfolio/src/master/"
        }
    ],
    "footerLinks": [
        {
            "id": "linkedin",
            "text": "Linked In",
            "href": "https://www.linkedin.com/in/andrewrevak/"
        },
        {
            "id": "photography",
            "text": "Photography Portfolio",
            "href": "http://utherwayn.smugmug.com/Travel/"
        },
        {
            "id": "bunnies",
            "text": "Bunnies!",
            "href": "https://rabbit.org/bunny-of-the-day/"
        }
    ]
} as IAppProps;

export default ProfileData;