import React from 'react';
import App from './App';
import renderer from 'react-test-renderer';

test('renders learn react link', () => {
  const defaultTestData = {
    "fullName": "Test User",
    "skills": [

    ],
    "articles": [

    ],
    "footerLinks": [
      {
        id: "linkedin",
        "text": "Linked In",
        "href": "https://www.linkedin.com/in/andrewrevak/"
      },
      {
        id: "photogrpahy",
        "text": "Photography Portfolio",
        "href": "http://utherwayn.smugmug.com/Travel/"
      },
      {
        id: "bunnies",
        "text": "Bunnies!",
        "href": "https://rabbit.org/bunny-of-the-day/"
      }
    ]
  } as IAppProps;
  const component = renderer.create(
      <App {...defaultTestData} />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  /*
  expect(queryByLabelText(/Linked In/)).toBeTruthy();
  expect(queryByLabelText(/Photography Portfolio/)).toBeTruthy();
  expect(queryByLabelText(/Bunnies!/)).toBeTruthy();
   */
});
