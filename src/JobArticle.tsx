import React from 'react';
import { DateTime } from 'luxon';
import ReactMarkdown from 'react-markdown';

const JobArticle: React.FC<JobArticle> = (props) => {
    let startDate = (props.startDate) ? DateTime.fromISO(props.startDate) : null;
    let endDate = (props.endDate) ? DateTime.fromISO(props.endDate) : null;
    let timeInPosition = 'Not specified';
    if (startDate && endDate) {
        timeInPosition = startDate.toFormat('MM/yyyy') + ' - ' + endDate.toFormat('MM/yyyy');
    } else if (endDate) {
        timeInPosition = endDate.toFormat('MM/yyyy');
    } else if (startDate) {
        timeInPosition = startDate.toFormat('MM/yyyy') + ' - Present';
    }

    return (
        <article>
            <h3>{props.title}</h3>
            <p>
                {props.company}<br />
                {timeInPosition}<br />
                <ReactMarkdown source={props.description} escapeHtml={false} />

            </p>
        </article>
    );
};

export default JobArticle;