import React, { ReactElement } from 'react';
import JobArticle from "./JobArticle";
import ProjectArticle from "./ProjectArticle";
import EducationArticle from "./EducationArticle";
import SkillArticle from "./SkillArticle";
import UnknownArticle from "./UnknownArticle";

import './App.scss';

function isWorkExperience(article: BaseArticle) : article is JobArticle {
    return (article as BaseArticle).articleType === "Job";
}

function isEducationArticle(article: BaseArticle) : article is EducationArticle {
    return (article as BaseArticle).articleType === "Education";
}

function isSkill(article: BaseArticle) : article is SkillArticle {
    return (article as BaseArticle).articleType === "Skill";
}

function isProject(article: BaseArticle) : article is ProjectArticle {
    return (article as BaseArticle).articleType === "Project";
}

function mapSkillToArticle(skill: Skill) : SkillArticle {
    return {
        articleType: "Skill",
        skillId: skill.id,
    } as SkillArticle;
}

function renderArticle(article: JobArticle|EducationArticle|ProjectArticle|SkillArticle|BaseArticle) : ReactElement {
    if (isWorkExperience(article)) {
        return <JobArticle key={article.id} {...article} />
    }

    if (isProject(article)) {
        return <ProjectArticle key={article.id} {...article} />
    }

    if (isEducationArticle(article)) {
        return <EducationArticle key={article.id} {...article} />
    }

    if (isSkill(article)) {
        return <SkillArticle key={article.id} {...article} />
    }
    
    return <UnknownArticle key={article.id} {...article} />
}

const App: React.FC<IAppProps> = (props) => {
    const articlesWorkExperience = props.articles.filter(isWorkExperience);
    const articlesEducation = props.articles.filter(isEducationArticle);
    const articlesSkills = props.skills.map(mapSkillToArticle);
    const articlesProjects = props.articles.filter(isProject);
    const sectionMap = [
        {
            heading: "Work Experience",
            articles: articlesWorkExperience,
            articleType: "Job",
            className: "App-work"
        },
        {
            heading: "Education",
            articles: articlesEducation,
            articleType: "Education",
            className: "App-education"
        },
        {
            heading: "Skills",
            articles: articlesSkills,
            articleType: "Skill",
            className: "App-skills"
        },
        {
            articleType: "Project",
            articles: articlesProjects,
            heading: "Projects",
            className: "App-projects"
        }
    ] as Array<SectionMapEntry>;

    return (
        <div className="App">
            <header className="App-header">
                <p>{props.fullName}</p>
            </header>

            { sectionMap.map(section => {
                return <section key={section.heading} className={section.className}>
                    <h2>{section.heading}</h2>

                    { section.articles.map((article) => renderArticle(article)) }
                </section>
            })}

            <footer className="App-footer">
                <ul className="App-footer-links">
                    {props.footerLinks.map(link => {
                        return <li key={link.id}><a href={link.href}>{link.text}</a></li>
                    })}
                </ul>
            </footer>
        </div>
    );
};

export default App;
