import React from 'react';
const ProjectArticle: React.FC<ProjectArticle> = (props) => {
    return (
        <article>
            <a href={props.href}>{props.title}</a>
        </article>
    );
};

export default ProjectArticle;
