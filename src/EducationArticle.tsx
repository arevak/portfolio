import React from 'react';
const EducationArticle: React.FC<EducationArticle> = (props) => {
    return (
        <article>
            <h3>{props.title}</h3>
            <p>
                <i>{props.degreeEarned}</i><br />
                Completed in {props.yearCompleted}
            </p>
        </article>
    );
};

export default EducationArticle;
