import React from 'react';
const SkillArticle: React.FC<SkillArticle> = (props) => {
    return (
        <article>
            <h3>{props.id}</h3>
        </article>
    );
};

export default SkillArticle;
